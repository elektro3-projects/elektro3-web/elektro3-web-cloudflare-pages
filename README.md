# elektro3-web-cloudflare-pages

The HTML pages that Cloudflare uses as templates to show errors when the site is down, when there is some restriction in place, a firewall rule triggers or some security events happen.
